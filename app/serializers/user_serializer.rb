class UserSerializer < ActiveModel::Serializer

    attributes :id,:first_name,:last_name

    has_one :mentor do
        link(:self) {user_mentor_path(object.id)}
    end
     
end