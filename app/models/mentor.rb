class Mentor < ApplicationRecord
    belongs_to :user 
    has_many :abilities
    has_many :expertises
    has_one :local
    has_one :public
end
