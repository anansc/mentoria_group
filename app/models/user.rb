class User < ApplicationRecord
    has_one :mentor
    has_one :social_network
end
