class CreateLocals < ActiveRecord::Migration[6.1]
  def change
    create_table :locals do |t|
      t.boolean :distrito 
      t.boolean :online
      t.boolean :presencial

      t.references :mentor, foreign_key: true
      
      t.timestamps
    end
  end
end
