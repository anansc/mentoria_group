class CreateExpertises < ActiveRecord::Migration[6.1]
  def change
    create_table :expertises do |t|
      t.string :name
      
      t.references :mentor, foreign_key: true

      t.timestamps
    end
  end
end
