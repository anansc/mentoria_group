class CreateUsers < ActiveRecord::Migration[6.1]
  def change
    create_table :users do |t|
      t.string :first_name
      t.string :last_name
      t.date :birthday
      t.string :email
      t.string :office
      t.string :gender
      t.string :adress
      t.string :description
      t.string :phone

      t.boolean :cpf
      t.boolean :cnpj

      t.timestamps
    end
  end
end
