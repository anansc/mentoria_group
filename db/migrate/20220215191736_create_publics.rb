class CreatePublics < ActiveRecord::Migration[6.1]
  def change
    create_table :publics do |t|
      t.boolean :cnpj
      t.boolean :cpf

      t.references :mentor, foreign_key: true

      t.timestamps
    end
  end
end
