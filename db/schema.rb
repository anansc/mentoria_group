# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2022_02_15_191736) do

  create_table "abilities", force: :cascade do |t|
    t.string "name"
    t.integer "mentor_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["mentor_id"], name: "index_abilities_on_mentor_id"
  end

  create_table "expertises", force: :cascade do |t|
    t.string "name"
    t.integer "mentor_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["mentor_id"], name: "index_expertises_on_mentor_id"
  end

  create_table "locals", force: :cascade do |t|
    t.boolean "distrito"
    t.boolean "online"
    t.boolean "presencial"
    t.integer "mentor_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["mentor_id"], name: "index_locals_on_mentor_id"
  end

  create_table "mentors", force: :cascade do |t|
    t.text "bio"
    t.integer "user_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["user_id"], name: "index_mentors_on_user_id"
  end

  create_table "publics", force: :cascade do |t|
    t.boolean "cnpj"
    t.boolean "cpf"
    t.integer "mentor_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["mentor_id"], name: "index_publics_on_mentor_id"
  end

  create_table "social_networks", force: :cascade do |t|
    t.string "linkedin", null: false
    t.string "facebook"
    t.string "instagram"
    t.integer "user_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["user_id"], name: "index_social_networks_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "first_name"
    t.string "last_name"
    t.date "birthday"
    t.string "email"
    t.string "office"
    t.string "gender"
    t.string "adress"
    t.string "description"
    t.string "phone"
    t.boolean "cpf"
    t.boolean "cnpj"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  add_foreign_key "abilities", "mentors"
  add_foreign_key "mentors", "users"
  add_foreign_key "publics", "mentors"
  add_foreign_key "social_networks", "users"
end
